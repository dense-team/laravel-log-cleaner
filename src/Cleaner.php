<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 20.11.2016
 * Time: 15:25
 */

namespace Dense\Log;

class Cleaner
{

    static public function logsDir()
    {
        return storage_path('logs');
    }

    static public function filter(array $files, $keep)
    {
        // find if it is laravel log file
        // laravel log files have mask laravel-YYYY-MM-DD.log
        // lumen log files have mask lumen-YYYY-MM-DD.log
        $logs = array_filter($files, function ($file) {
            return (strpos($file, 'laravel') === 0 || strpos($file, 'lumen') === 0);
        });

        // sort files from newest do oldest
        rsort($logs);

        // split files based on number files desired to keep
        $logsToDelete = array_slice($logs, (int)$keep);

        return $logsToDelete;
    }

    static public function clear(array $logs, $keep)
    {
        $logs = self::filter($logs, $keep);

        $logsDir = Cleaner::logsDir();

        foreach ($logs as $log) {
            $logPath = $logsDir . DIRECTORY_SEPARATOR . $log;

            if (file_exists($logPath)) {
                unlink($logPath);
            }
        }
    }
}