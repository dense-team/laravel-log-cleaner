<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 20.11.2016
 * Time: 14:20
 */

namespace Dense\Log\Command;

use Illuminate\Console\Command;

use Dense\Log\Cleaner;

class LogClear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:logs {keep}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears all logs.';

    /**
     * Execute the console command.
     *
     * @return string
     */
    public function handle()
    {
        $keep = $this->argument('keep');

        // logs directory
        $logsDir = Cleaner::logsDir();

        // all files in logs directory
        $files = scandir($logsDir);

        // filter and clear log files
        Cleaner::clear($files, $keep);

        $this->info('Log files were cleared successfully.');
    }
}