<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 20.11.2016
 * Time: 16:18
 */


use Dense\Log\Cleaner;

class ParserFactoryTest extends PHPUnit_Framework_TestCase
{
    public function tearDown()
    {
        Mockery::close();
    }

    protected function getFiles()
    {
        return [
            '.gitkeep',
            'laravel-2016-12-31.log',
            'tmp2184983541',
            'laravel-2015-07-15.log',
            'laravel-2016-12-30.log',
            'laravel-2016-11-17.log',
            'tmp2184983541',
            'tmp2184983541',
            'laravel-2015-12-01.log',
            'laravel-2016-09-04.log',
            'laravel-2016-11-22.log',
            'tmp2184983541',
            'laravel-2016-01-01.log',
            'laravel-2016-03-04.log',
            'laravel-2016-05-17.log',
            'tmp2184983541',
            'laravel-2016-08-18.log',
        ];
    }

    public function testLogsFiltering()
    {
        $files = $this->getFiles();

        $keep = 0;

        $actual = Cleaner::filter($files, $keep);

        // to be deleted
        $expected = [
            'laravel-2016-12-31.log',
            'laravel-2016-12-30.log',
            'laravel-2016-11-22.log',
            'laravel-2016-11-17.log',
            'laravel-2016-09-04.log',
            'laravel-2016-08-18.log',
            'laravel-2016-05-17.log',
            'laravel-2016-03-04.log',
            'laravel-2016-01-01.log',
            'laravel-2015-12-01.log',
            'laravel-2015-07-15.log',
        ];

        $this->assertEquals($actual, $expected);
    }

    public function testLogsFilteringWithKeep()
    {
        $files = $this->getFiles();

        $keep = 7;

        $actual = Cleaner::filter($files, $keep);

        // to be deleted
        $expected = [
            'laravel-2016-03-04.log',
            'laravel-2016-01-01.log',
            'laravel-2015-12-01.log',
            'laravel-2015-07-15.log',
        ];

        $this->assertEquals($actual, $expected);
    }
}
