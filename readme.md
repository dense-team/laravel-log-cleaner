# Log cleaner for Lavarel by Dense

Simple package containing console command for clearing daily logs.

## Instalation

Run following composer command.

```shell
composer require dense/log-cleaner
```

Add following code to app\Console\Kernel.php.
```php
protected $commands = [
    ... 
    
    \Dense\Log\Command\LogClear::class,
];

protected function schedule(Schedule $schedule)
{
    ...

    $schedule->command('clear:logs 7')
        ->dailyAt('01:00');
}
```

## Configuration

Command clear:logs takes one argument, which is an integer representing amount of log files yout want to keep in your logs directory. Rest of them will be deleted.

I put value 7 in schedule method to keep last week of log files on the disk, just to be sure. However feel free to modify that number to your needs.

Also do not forget to set up cron for scheduled commands. You can read about that in the official Laravel documentation.

## Versioning

Version 1.* requires php 5.5 and is suitable for Laravel or Lumen version 5.2 and less.   
Version 2.* requires php 5.6 or higher and is suitable for Laravel or Lumen version 5.3. 

## Frequently asked questions

Q: Why is this needed?  
A: Performance. On websites where error logs are added on daily basis, log directory may grow quickly and can cause performance issues on read/write operations.  
 
Q: What if I set my logs to be stored in single file?  
A: Performance issues again. When your single log file grows, it will become slower and slower to write into.  

Q: Why is this not integrated in Laravel already?  
A: I do not know, I keep asking myself the same question.  
